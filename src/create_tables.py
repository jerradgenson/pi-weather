"""
Use SQLAlchemy to create tables in the database.

Copyright 2019 Jerrad Michael Genson
This file is part of Pi Weather.

Pi Weather is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Pi Weather is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Pi Weather.  If not, see <https://www.gnu.org/licenses/>.

"""


from pathlib import Path

from sqlalchemy import create_engine

from data import weather_observations
from data.constants import WEATHER_OBSERVATIONS_DATABASE_URL


def main():
    print('Creating new database with empty tables')
    engine = create_engine(WEATHER_OBSERVATIONS_DATABASE_URL)
    weather_observations.Base.metadata.create_all(engine)
    print('Database created successfully')


if __name__ == '__main__':
    main()
