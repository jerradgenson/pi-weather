import multiprocessing
from random import random

import sklearn
import numpy as np

DEFAULT_C1 = 2
DEFAULT_C2 = DEFAULT_C1
DEFAULT_VMAX = 1
KEEP_GOING_INCREMENTS = 5


def _child_process(in_queue, out_queue, func):
    while True:
        index, arg = in_queue.get()
        out_queue.put((index, func(arg)))


def create_concurrent_func(func, process_count):
    def apply(args):
        for max_index, arg in enumerate(args):
            out_queue.put((max_index, arg))

        processed_args = {}
        current_index = 0
        while current_index < max_index:
            try:
                yield processed_args[current_index]
                current_index = current_index + 1

            except KeyError:
                index, arg = in_queue.get()
                processed_args[index] = arg

    out_queue = multiprocessing.Queue()
    in_queue = multiprocessing.Queue()
    for _ in range(process_count):
        process = multiprocessing.Process(target=_child_process,
                                          args=(out_queue, in_queue, func))

        process.daemon = True
        process.start()

    return apply


def search(initial_solutions,
           measure_fitness,
           c1=DEFAULT_C1,
           c2=DEFAULT_C2,
           vmax=DEFAULT_VMAX,
           max_iterations=-1,
           processes=multiprocessing.cpu_count(),
           verbose=False):
    if verbose:
        print('Creating subprocesses')

    measure_fitness_conc = create_concurrent_func(measure_fitness, processes)
    particles = []
    if verbose:
        print('Creating initial particles')

    initial_velocity = np.zeros(len(initial_solutions[0]))
    for solution in initial_solutions:
        particle = Particle(solution, solution, 0, initial_velocity)
        particles.append(particle)

    gbest = None
    keep_going = True
    count = 0
    while keep_going or (count + 1) % KEEP_GOING_INCREMENTS != 0 and count != max_iterations:
        count = count + 1
        if verbose:
            print('Iteration #{}'.format(count))

        if count % KEEP_GOING_INCREMENTS == 0:
            keep_going = False

        for particle, fitness in zip(particles, measure_fitness_conc(p.position for p in particles)):
            if fitness > particle.pbest_fitness:
                particle.pbest = particle.position
                particle.pbest_fitness = fitness
                keep_going = True
                if verbose:
                    print('New personal best: ' + str(particle.position))

            if gbest is None or fitness > gbest[1]:
                gbest = particle.position, fitness
                keep_going = True
                if verbose:
                    print('New global best: ' + str(particle.position))

        print('Calculating new particle positions')
        for particle in particles:
            particle.velocity = (particle.velocity
                                 + c1
                                 * random()
                                 * (particle.pbest - particle.position)
                                 + c2
                                 * random()
                                 * (gbest[0] - particle.position))

            particle.velocity = np.clip(particle.velocity, -vmax, vmax)
            particle.position = particle.position + particle.velocity

    return gbest


class Particle:
    def __init__(self, position, pbest, pbest_fitness, velocity):
        self.position = position
        self.pbest = pbest
        self.pbest_fitness = pbest_fitness
        self.velocity = velocity
