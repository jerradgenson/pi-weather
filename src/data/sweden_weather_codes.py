"""
Contains Swedish weather code data and a function to map Swedish codes
to the generic codes in the Weather Observations database.

Copyright 2019 Jerrad Michael Genson
This file is part of Pi Weather.

Pi Weather is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Pi Weather is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Pi Weather.  If not, see <https://www.gnu.org/licenses/>.

"""

import json
from pathlib import Path

from data import constants

_SWEDEN_WEATHER_CODES_JSON = constants.PROJECT_ROOT / Path('src/data/sweden_weather_codes.json')
with open(_SWEDEN_WEATHER_CODES_JSON) as fp:
    CODES = json.load(fp)


def map_code(swedish_weather_code):
    """
    Map a Swedish weather code to the corresponding generic code in the Weather
    Observations database. Return None if a mapping for a particular code is
    not found.

    Args
      swedish_weather_codes: A single numeric weather code.

    Returns
      A mapped weather code or None.

    """

    try:
        return CODES[str(swedish_weather_code)]

    except KeyError:
        return None
