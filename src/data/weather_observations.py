"""
SQL Alchemy object relational mapper definitions

Copyright 2019 Jerrad Michael Genson
This file is part of Pi Weather.

Pi Weather is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Pi Weather is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Pi Weather.  If not, see <https://www.gnu.org/licenses/>.

"""

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float, ForeignKey, DateTime
from sqlalchemy.orm import relationship

Base = declarative_base()


class Observation(Base):
    __tablename__ = 'observation'

    id = Column(Integer, primary_key=True)
    datetime = Column(DateTime, nullable=False)
    temperature = Column(Float)
    pressure = Column(Float)
    relative_humidity = Column(Float)
    rainfall = Column(Float)
    irradiance = Column(Float)
    wind_speed = Column(Integer)
    wind_direction = Column(Integer)
    stability_class_id = Column(Integer, ForeignKey('stability_class.id'))
    stability_class = relationship('StabilityClass', back_populates='observations')
    prevailing_weather_id = Column(Integer, ForeignKey('prevailing_weather.id'))
    prevailing_weather = relationship('PrevailingWeather', back_populates='observations')
    station_id = Column(Integer, ForeignKey('station.id'), nullable=False)
    station = relationship('Station', back_populates='observations')


class Station(Base):
    __tablename__ = 'station'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    latitude = Column(Float)
    longitude = Column(Float)
    elevation = Column(Float)
    station_number = Column(Integer)
    climate_id = Column(Integer, ForeignKey('climate.id'))
    climate = relationship('Climate', back_populates='stations')
    country_id = Column(Integer, ForeignKey('country.id'), nullable=False)
    country = relationship('Country', back_populates='stations')
    city_id = Column(Integer, ForeignKey('city.id'))
    city = relationship('City', back_populates='stations')
    station_type_id = Column(Integer,
                             ForeignKey('station_type.id'),
                             nullable=False)

    station_type = relationship('StationType', back_populates='stations')
    observations = relationship('Observation',
                                order_by=Observation.id,
                                back_populates='station')


class StationType(Base):
    __tablename__ = 'station_type'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    stations = relationship('Station',
                            order_by=Station.id,
                            back_populates='station_type')


class Country(Base):
    __tablename__ = 'country'

    id = Column(Integer, primary_key=True)
    code = Column(String, nullable=False, unique=True)
    name = Column(String, nullable=False, unique=True)
    stations = relationship('Station',
                            order_by=Station.id,
                            back_populates='country')


class City(Base):
    __tablename__ = 'city'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    stations = relationship('Station',
                            order_by=Station.id,
                            back_populates='city')


class Climate(Base):
    __tablename__ = 'climate'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    stations = relationship('Station',
                            order_by=Station.id,
                            back_populates='climate')


class StabilityClass(Base):
    __tablename__ = 'stability_class'

    id = Column(Integer, primary_key=True)
    classification = Column(String, nullable=False)
    observations = relationship('Observation',
                                order_by=Observation.id,
                                back_populates='stability_class')


class PrevailingWeather(Base):
    __tablename__ = 'prevailing_weather'

    id = Column(Integer, primary_key=True)
    description = Column(String, nullable=False)
    observations = relationship('Observation',
                                order_by=Observation.id,
                                back_populates='prevailing_weather')
