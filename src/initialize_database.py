"""
Inserts database entries to initialize the Weather Observations database.

Copyright 2019 Jerrad Michael Genson
This file is part of Pi Weather.

Pi Weather is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Pi Weather is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Pi Weather.  If not, see <https://www.gnu.org/licenses/>.

"""

import sys

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from data import weather_observations
from data import constants


Session = sessionmaker()


def main():
    print('Initializing database with base entries')
    engine = create_engine(constants.WEATHER_OBSERVATIONS_DATABASE_URL)
    Session.configure(bind=engine)
    session = Session()

    sweden = weather_observations.Country(name='Sweden', code='se')
    gothenburg = weather_observations.City(name='Gothenburg')
    humid_continental = weather_observations.Climate(name='humid continental')
    weather_station = weather_observations.StationType(name='weather')
    solar_station = weather_observations.StationType(name='solar')
    hybrid_station = weather_observations.StationType(name='hybrid')
    session.add_all([sweden,
                     gothenburg,
                     humid_continental,
                     weather_station,
                     solar_station,
                     hybrid_station])

    session.commit()

    goteborg_a = weather_observations.Station(name='Göteborg A',
                                              latitude=57.7156,
                                              longitude=11.9924,
                                              elevation=3.038,
                                              climate=humid_continental,
                                              country=sweden,
                                              city=gothenburg,
                                              station_number=71420,
                                              station_type=weather_station)

    session.add(goteborg_a)
    session.commit()

    goteborg_sol = weather_observations.Station(name='Göteborg Sol',
                                                latitude=57.6879,
                                                longitude=11.9797,
                                                elevation=94,
                                                climate=humid_continental,
                                                country=sweden,
                                                city=gothenburg,
                                                station_number=71415,
                                                station_type=solar_station)

    session.add(goteborg_sol)
    session.commit()

    landvetter = weather_observations.Station(name='Landvetter Flygplats',
                                              latitude=57.6764,
                                              longitude=12.2919,
                                              elevation=154,
                                              climate=humid_continental,
                                              country=sweden,
                                              city=gothenburg,
                                              station_number=72420,
                                              station_type=weather_station)

    session.add(landvetter)
    session.commit()

    clear = weather_observations.PrevailingWeather(description='clear')
    session.add(clear)
    session.commit()

    rain = weather_observations.PrevailingWeather(description='rain')
    session.add(rain)
    session.commit()

    downpour = weather_observations.PrevailingWeather(description='downpour')
    session.add(downpour)
    session.commit()

    fog = weather_observations.PrevailingWeather(description='fog')
    session.add(fog)
    session.commit()

    snow = weather_observations.PrevailingWeather(description='snow and ice')
    session.add(snow)
    session.commit()

    strong_wind = weather_observations.PrevailingWeather(description='strong wind')
    session.add(strong_wind)
    session.commit()

    storm = weather_observations.PrevailingWeather(description='storm')
    session.add(storm)
    session.commit()

    winter_storm = weather_observations.PrevailingWeather(description='winter_storm')
    session.add(winter_storm)
    session.commit()

    tornado = weather_observations.PrevailingWeather(description='tornado')
    session.add(tornado)
    session.commit()

    hurricane = weather_observations.PrevailingWeather(description='hurricane')
    session.add(hurricane)
    session.commit()

    print('Database initialized successfully')

    return 0


if __name__ == '__main__':
    sys.exit(main())
