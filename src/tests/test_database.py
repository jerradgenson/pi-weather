"""
Test the veracity and integrity of the database.

Copyright 2019 Jerrad Michael Genson
This file is part of Pi Weather.

Pi Weather is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Pi Weather is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Pi Weather.  If not, see <https://www.gnu.org/licenses/>.

"""


import unittest
from datetime import datetime

from sqlalchemy import create_engine, text
from sqlalchemy.orm import sessionmaker

from data import constants
from data.weather_observations import Observation

TEST_DUPLICATES_SQL_QUERY = 'SELECT datetime, station_id, COUNT(*) FROM observation GROUP BY datetime, station_id HAVING COUNT(*) > 1'


Session = sessionmaker(bind=create_engine(constants.WEATHER_OBSERVATIONS_DATABASE_URL))


class TestDatabase(unittest.TestCase):
    def test_goteborg_a_20190417_000000(self):
        session = Session()
        obs = session.query(Observation).\
            filter_by(station_id=1).\
            filter_by(datetime=datetime(2019, 4, 17, 0, 0, 0)).one()

        self.assertAlmostEqual(obs.temperature, 3.1)
        self.assertAlmostEqual(obs.pressure, 1032.9)
        self.assertAlmostEqual(obs.relative_humidity, 59.0)
        self.assertAlmostEqual(obs.wind_speed, 2.1)
        self.assertEqual(obs.wind_direction, 40)
        self.assertAlmostEqual(obs.rainfall, 0.0)
        self.assertEqual(obs.prevailing_weather_id, 1)

    def test_goteborg_a_20141227_010000(self):
        session = Session()
        obs = session.query(Observation).\
            filter_by(station_id=1).\
            filter_by(datetime=datetime(2014, 12, 27, 1, 0, 0)).one()

        self.assertAlmostEqual(obs.temperature, 0.5)
        self.assertAlmostEqual(obs.pressure, 1013.7)
        self.assertAlmostEqual(obs.relative_humidity, 94.0)
        self.assertAlmostEqual(obs.wind_speed, 2.0)
        self.assertEqual(obs.wind_direction, 215)
        self.assertAlmostEqual(obs.rainfall, 3.8)
        self.assertEqual(obs.prevailing_weather_id, 5)

    def test_goteborg_sol_20190828_160000(self):
        session = Session()
        obs = session.query(Observation).\
            filter_by(station_id=2).\
            filter_by(datetime=datetime(2019, 8, 28, 16, 0, 0)).one()

        self.assertAlmostEqual(obs.irradiance, 209.27)

    def test_goteborg_sol_20080103_100000(self):
        session = Session()
        obs = session.query(Observation).\
            filter_by(station_id=2).\
            filter_by(datetime=datetime(2008, 1, 3, 10, 0, 0)).one()

        self.assertAlmostEqual(obs.irradiance, 19.89)

    def test_landvetter_20190421_000000(self):
        session = Session()
        obs = session.query(Observation).\
            filter_by(station_id=3).\
            filter_by(datetime=datetime(2019, 4, 21, 0, 0, 0)).one()

        self.assertAlmostEqual(obs.temperature, 4.8)
        self.assertAlmostEqual(obs.pressure, 1028.5)
        self.assertAlmostEqual(obs.relative_humidity, 57.0)
        self.assertAlmostEqual(obs.wind_speed, 2.0)
        self.assertEqual(obs.wind_direction, 220)
        self.assertEqual(obs.prevailing_weather_id, 1)

    def test_landvetter_20150730_120000(self):
        session = Session()
        obs = session.query(Observation).\
            filter_by(station_id=3).\
            filter_by(datetime=datetime(2015, 7, 30, 12, 0, 0)).one()

        self.assertAlmostEqual(obs.temperature, 14.2)
        self.assertAlmostEqual(obs.pressure, 1000.6)
        self.assertAlmostEqual(obs.relative_humidity, 95.0)
        self.assertAlmostEqual(obs.wind_speed, 3.0)
        self.assertEqual(obs.wind_direction, 260)
        self.assertEqual(obs.prevailing_weather_id, 3)

    def test_no_duplicates(self):
        session = Session()
        obs = session.query(Observation).from_statement(text(TEST_DUPLICATES_SQL_QUERY)).all()
        self.assertEqual(len(obs), 0)


if __name__ == '__main__':
    unittest.main()
