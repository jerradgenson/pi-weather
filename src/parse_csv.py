"""
Parse CSV files containing weather data from a
single SMHI weather station.

Copyright 2019 Jerrad Michael Genson
This file is part of Pi Weather.

Pi Weather is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Pi Weather is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Pi Weather.  If not, see <https://www.gnu.org/licenses/>.

"""


import sys
from datetime import date, time, datetime
from pathlib import Path

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from data.weather_observations import Observation
from data import constants
from data.sweden_weather_codes import map_code

CSV_DIR = constants.PROJECT_ROOT / Path('data/raw_cmhi_data/')
STATION_ID_FILE = Path('station_id')


def main():
    measurements = {}
    for path in CSV_DIR.iterdir():
        if path.is_dir():
            measurements.update(process_directory(path))

    # Insert new data into the database.
    engine = create_engine(constants.WEATHER_OBSERVATIONS_DATABASE_URL)
    Session = sessionmaker(bind=engine)
    session = Session()

    # Convert measurement dicts into Observation instances.
    print('Adding {} observation rows to database session'.format(len(measurements)))
    session.add_all(Observation(**measurement) for measurement in measurements.values())
    print('Commiting database session')
    session.commit()
    print('Database updated successfully')


def process_directory(directory):
    with (directory / STATION_ID_FILE).open() as station_id_fp:
        station_id = int(station_id_fp.readline())

    # Process all CSV files in target directory.
    measurements = {}
    for csv_file in directory.iterdir():
        if csv_file.suffix != '.csv':
            continue

        print('Parsing {} with parser: '.format(csv_file), end='')
        with csv_file.open() as fp:
            csv_data = fp.read()

        success = False
        for parse in parsers:
            success = parse(csv_data, measurements, station_id)
            if success:
                break

        if not success:
            raise ValueError('`{}` not a known file. Can not parse.'.format(csv_file))

    return measurements


def create_parser(key,
                  measurement_qualities,
                  elements_mapping,
                  postprocess=None):
    def parser(csv_data, measurements, station_id):
        if key not in csv_data:
            return False

        print(key)
        found_data = False
        observations_extracted = 0
        for row in csv_data.strip().split('\n'):
            if found_data:
               elements = row.split(';')

               # 'G' means the measurement's accuracy  has been verified.
               # 'Y' means the measurement is suspicious.
               if any(elements[col] != 'G' for col in measurement_qualities):
                   continue

               date = parse_date(elements[0])
               time = parse_time(elements[1])
               measurement = {name: elements[index] for index, name in elements_mapping.items()}
               if postprocess:
                   postprocess(measurement)

               insert_measurement(measurement, measurements, date, time, station_id)
               observations_extracted += len(measurement)

            elif 'Datum;Tid (UTC)' in row:
                found_data = True

        print('Extracted {} observations'.format(observations_extracted))
        return True

    return parser


def insert_measurement(measurement,
                       measurements,
                       date,
                       time,
                       station_id):
    datetime_station = date.isoformat() + time.isoformat() + str(station_id)
    try:
        measurements[datetime_station].update(measurement)

    except KeyError:
        measurement['station_id'] = station_id
        measurement['datetime'] = datetime.combine(date, time)
        measurements[datetime_station] = measurement


def parse_date(date_string):
    return date(*[int(x) for x in date_string.split('-')])


def parse_time(time_string):
    return time(*[int(x) for x in time_string.split(':')])


parse_humidity = create_parser('Relativ Luftfuktighet',
                               (3,),
                               {2: 'relative_humidity'})


parse_pressure = create_parser('Lufttryck',
                               (3,),
                               {2: 'pressure'})

_key = 'prevailing_weather_id'
parse_present_weather = create_parser('Present weather',
                                      (3,),
                                      {2: _key},
                                      lambda x: x.update({_key: map_code(x[_key])}))


parse_rainfall = create_parser('Nederb',
                               (3,),
                               {2: 'rainfall'})


parse_temperature = create_parser('Lufttemperatur',
                                  (3,),
                                  {2: 'temperature'})


parse_wind_profile = create_parser('Vindriktning',
                                   (3, 5),
                                   {2: 'wind_direction', 4: 'wind_speed'})


parse_irradiance = create_parser('Global Irradians',
                                 (3,),
                                 {2: 'irradiance'})


parsers = (parse_humidity,
           parse_irradiance,
           parse_pressure,
           parse_present_weather,
           parse_rainfall,
           parse_temperature,
           parse_wind_profile)


if __name__ == '__main__':
    sys.exit(main())
