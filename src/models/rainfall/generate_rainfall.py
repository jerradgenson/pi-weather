"""
Construct a machine learning model to predict whether or not
there will be rain.

Copyright 2019 Jerrad Michael Genson
This file is part of Pi Weather.

Pi Weather is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Pi Weather is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Pi Weather.  If not, see <https://www.gnu.org/licenses/>.

"""

import pickle
from multiprocessing import Pool, cpu_count
from datetime import date, datetime, timedelta
from collections import namedtuple
from pathlib import Path

import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from data import constants
from data.weather_observations import Observation, Station

# Minimum amount of rainfall that the model should predict for, in cm.
MINIMUM_RAINFALL = 0

# Sets of times of datapoints before precipitation begins to feed into the
# model, in hours. Ranges from 1 to 24 hours before precipitation begins
# (for the first entry in the time series).
TIME_SERIES_SETS = tuple(((x, x+2) for x in range(1, 25)))

# Number of times to regenerate the model when finding the best estimator.
BEST_ESTIMATOR_ROUNDS = 10

# Amounts of rainfall (in mm) to train estimators to forecast.
RAINFALL_AMOUNTS = 0, 2.5, 10

# Used to seed random number generator in build_model.
RANDINT_UPPER = 100000

# Decimal precision to use when printing model performance statistics.
STATISTICS_PRECISION = 3

# Use all datapoints before this date as training data (non-inclusive).
TRAINING_END_DATE = date(2018, 1, 1)

# Use all datapoints after this data as test data (inclusive).
TESTING_START_DATE = TRAINING_END_DATE

MODEL_PERSISTENCE_FILE = constants.PROJECT_ROOT / Path('data/rainfall.dat')

# Path to the PID file when running as a daemon.
PID_FILE = '/var/tmp/generate_rainfall.pid'

# Filename to write model performance statictics to when running as a daemon.
TEST_RESULTS_FILENAME = Path('{}_test_results'.format(datetime.now().strftime('%Y%m%d%H%M%S')))

# Path to write model performance statictics to when running as a daemon.
TEST_RESULTS_PATH = constants.PROJECT_ROOT / Path('data') / TEST_RESULTS_FILENAME

# Hyperparameters to use for support vector machine.
SVM_HYPERPARAMETERS = {
    'C': 1000,
    'cache_size': 500,
    'class_weight': {-1.0: 1, 1.0: 1},
    'coef0': 0.0,
    'decision_function_shape': 'ovr',
    'degree': 3,
    'gamma': 0.01,
    'kernel': 'rbf',
    'max_iter': -1,
    'probability': False,
    'random_state': None,
    'shrinking': True,
    'tol': 0.001,
    'verbose': False
}


Datasets = namedtuple('Datasets', ['inputs', 'targets', 'scale'])

ModelResults = namedtuple('ModelResults',
                          ['accuracy',
                           'precision',
                           'sensitivity',
                           'specificity'])


def generate_complete_model(n_jobs=cpu_count()):
    pool = Pool(n_jobs)
    jobs = []
    for time_series in TIME_SERIES_SETS:
        for rainfall_amount in RAINFALL_AMOUNTS:
            jobs.append((time_series, rainfall_amount))

    rainfall_estimators = pool.map(generate_estimator, jobs)
    pool.close()
    forecast_periods = {}
    for time_series_count, time_series in enumerate(TIME_SERIES_SETS):
        rainfall_amounts = {}
        for rainfall_amount_count, rainfall_amount in enumerate(RAINFALL_AMOUNTS):
            index = time_series_count * len(RAINFALL_AMOUNTS) + rainfall_amount_count
            rainfall_amounts[rainfall_amount] = rainfall_estimators[index]

        forecast_periods[time_series[0]] = rainfall_amounts

    with MODEL_PERSISTENCE_FILE.open('wb') as model_persistance_file:
        pickle.dump(forecast_periods, model_persistance_file)


def generate_estimator(time_series_rainfall_amount):
    time_series, rainfall_amount = time_series_rainfall_amount
    print('Time series: {}'.format(time_series))
    print('Rainfall amount: {}'.format(rainfall_amount))

    # Query database for training and testing data.
    training_rainfall_data, training_no_rainfall_data, training_goteborg_a_data =\
        query_dataset(rainfall_amount, end_date=TRAINING_END_DATE)

    testing_rainfall_data, testing_no_rainfall_data, testing_goteborg_a_data =\
        query_dataset(rainfall_amount, start_date=TESTING_START_DATE)

    # Align datasets along a time series.
    training_rainfall_data = align_measurements(training_goteborg_a_data,
                                                training_rainfall_data,
                                                time_series)

    training_no_rainfall_data = align_measurements(training_goteborg_a_data,
                                                   training_no_rainfall_data,
                                                   time_series)

    testing_rainfall_data = align_measurements(testing_goteborg_a_data,
                                               testing_rainfall_data,
                                               time_series)

    testing_no_rainfall_data = align_measurements(testing_goteborg_a_data,
                                                  testing_no_rainfall_data,
                                                  time_series)

    # Apply preprocessing to testing datasets.
    testing_data = preprocess_data(testing_rainfall_data,
                                   testing_no_rainfall_data)

    static_part = training_rainfall_data, training_no_rainfall_data, testing_data.scale
    jobs = (static_part + (np.random.randint(RANDINT_UPPER),) for _ in range(BEST_ESTIMATOR_ROUNDS))
    models = map(train_svm, jobs)

    best_model = None
    for test_round, model in enumerate(models):
        skill = test_model(model, testing_data)
        if best_model is None or skill.accuracy > best_model[1].accuracy:
            best_model = model, skill

    return best_model


def train_svm(data_scale_seed):
    rainfall_data, no_rainfall_data, scale, seed = data_scale_seed
    np.random.seed(seed)
    training_data = preprocess_data(rainfall_data, no_rainfall_data, scale=scale)
    svm = SVC(**SVM_HYPERPARAMETERS)
    svm.fit(training_data.inputs, training_data.targets)

    return svm


def test_model(classifier, testing_dataset):
    """
    Test a classifier (model) on testing data and print performance statistics.

    """

    predictions = classifier.predict(testing_dataset.inputs)
    accuracy = 1 - np.mean(predictions != testing_dataset.targets)
    false_positives = np.sum(np.logical_and(predictions == 1,
                                            testing_dataset.targets == -1))

    true_positives = np.sum(np.logical_and(predictions == 1,
                                           testing_dataset.targets == 1))

    false_negatives = np.sum(np.logical_and(predictions == -1,
                                            testing_dataset.targets == 1))

    true_negatives = np.sum(np.logical_and(predictions == -1,
                                           testing_dataset.targets == -1))

    sensitivity = true_positives / (true_positives + false_negatives)
    specificity = true_negatives / (true_negatives + false_positives)
    precision = true_positives / (true_positives + false_positives)

    return ModelResults(accuracy, precision, sensitivity, specificity)


def preprocess_data(rainfall_data, no_rainfall_data, scale=None):
    """
    Apply Stage 2 preprocessing to rainfall / no rainfall arrays.
    This preprocessing includes:
      - Equalizine data so we have the same number of rainfall and no
        rainfall datapoints.
      - Replace rainfall measurements with a 1 to indicate rainfall, or
        a -1 to indicate no rainfall.
      - Combine rainfall and no rainfall data into a single dataset.
      - Randomize the order of datapoints in the dataset.
      - Scaling the combined datasets
      - Separate datasets into inputs and targets.

    Returns
      An instance of Datasets with fully-preprocessed data that is
      ready to be used for machine learning.

    """

    # Equalize data so we have the same number of rainfall and
    # no rainfall datapoints.
    training_sample_size = min((len(rainfall_data.index),
                                len(no_rainfall_data.index)))

    if len(no_rainfall_data.index) > training_sample_size:
        no_rainfall_data = no_rainfall_data.sample(training_sample_size)

    elif len(rainfall_data) > training_sample_size:
        rainfall_data = rainfall_data.sample(training_sample_size)

    # Replace rainfall measurements with 1 to indicate rainfall, and
    # -1 to indicate no rainfall.
    rainfall_data = rainfall_data.assign(rainfall=1)
    rainfall_data.astype({'rainfall': np.int}, copy=False)
    no_rainfall_data = no_rainfall_data.assign(rainfall=-1)
    no_rainfall_data.astype({'rainfall': np.int}, copy=False)

    # Combine rainfall and no rainfall data into a single dataset.
    combined_frames = rainfall_data.append(no_rainfall_data)

    # Randomize the order of datapoints in the datasets.
    combined_frames = combined_frames.sample(frac=1).reset_index(drop=True)

    # Separate datasets into inputs and targets.
    inputs = combined_frames.drop(['rainfall'], axis=1).to_numpy()
    targets = combined_frames[['rainfall']].to_numpy()
    targets = np.ravel(targets)

    if not scale:
        scaler = StandardScaler().fit(inputs)
        scale = scaler.transform

    inputs = scale(inputs)

    return Datasets(inputs, targets, scale)


def query_dataset(rainfall_amount, start_date=None, end_date=None):
    """
    Query the database for a dataset within a given time period.

    Optional
      start_date: The earliest datapoint to include in the dataset. If None,
                  then no boundary is placed on the earliest datapoint.
                  Default is None.

      end_date: The latest datapoint to include in the dataset. If None,
                then no boundary is placed on the latest datapoint.
                Default is None.

    Returns
      A tuple of SQLAlchemy Query objects representing records containing
      rainfall, records not containing rainfall, and all records from the
      Goteborg A station within the given time period.

    """

    engine = create_engine(constants.WEATHER_OBSERVATIONS_DATABASE_URL)
    session = sessionmaker(bind=engine)()
    rainfall = query_rainfall(rainfall_amount, session, start_date, end_date)
    no_rainfall = query_no_rainfall(rainfall_amount, session, start_date, end_date)
    goteborg_a = query_goteborg_a(session)
    session.close()

    return rainfall, no_rainfall, goteborg_a


def query_goteborg_a(session):
    """
    Query the database for all records from weather station Goteborg A
    where a measurement for temperature, pressure, and relative
    humidity are all present.

    Args
      session: A SQLAlchemy Session object.

    Returns
      A SQLAlchemy Query object.

    """

    goteborg_a_id = query_goteborg_a_id(session)
    query = session.query(Observation.datetime,
                          Observation.temperature,
                          Observation.pressure,
                          Observation.relative_humidity).\
        filter(Observation.temperature != None).\
        filter(Observation.pressure != None).\
        filter(Observation.relative_humidity != None).\
        filter(Observation.station_id == goteborg_a_id)

    return pd.read_sql(query.statement, session.bind,
                       parse_dates=['datetime'],
                       index_col='datetime')


def query_goteborg_a_id(session):
    """  Return the id of Goteborg A.  """

    return session.query(Station.id).filter_by(name='Göteborg A').one()[0]


def query_rainfall(rainfall_amount, session, start_date=None, end_date=None):
    """
    Return all observations where the rainfall is greater than 0.

    """

    query = session.query(Observation.datetime, Observation.rainfall).\
        filter(Observation.rainfall > rainfall_amount)

    if start_date:
        query = query.filter(Observation.datetime >= start_date)

    if end_date:
        query = query.filter(Observation.datetime < end_date)

    return pd.read_sql(query.statement, session.bind,
                       parse_dates=['datetime'],
                       index_col='datetime')


def query_no_rainfall(rainfall_amount, session, start_date=None, end_date=None):
    """
    Return all observations where the rainfall is equal to 0.

    """

    query = session.query(Observation.datetime, Observation.rainfall).\
        filter(Observation.rainfall <= rainfall_amount)

    if start_date:
        query = query.filter(Observation.datetime >= start_date)

    if end_date:
        query = query.filter(Observation.datetime < end_date)

    return pd.read_sql(query.statement, session.bind,
                       parse_dates=['datetime'],
                       index_col='datetime')


def align_measurements(input_measurements, target_measurements, time_series):
    """
    Align input_measurements (containing temperature, humidity, and pressure)
    with target_measurements (containing rainfall amount) using a time series
    such that each target measurement is joined with one or more input
    measurements from the past, as specified by the time series.

    Args
      input_measurements: A Pandas dataframe with columns for datetime,
                          temperature, pressure, and humidity.
      rainfall_measurements: A Pandas dataframe with columns for datetime and
                             rainfall amount.

    Returns
      A Pandas dataframe with a set of temperature, humidity, and pressure
      columns for each point in the time series, a single rainfall column,
      and a datetime index corresponding to the rainfall column.

    """

    aligned_measurements = target_measurements.copy()
    for hours in time_series:
        shifted_inputs = input_measurements.tshift(freq=timedelta(hours=-hours))
        aligned_measurements = aligned_measurements.join(shifted_inputs,
                                                         how='inner',
                                                         rsuffix=str(hours))

    return aligned_measurements


if __name__ == '__main__':
    generate_complete_model()
