#!/bin/bash

rm ../data/weather_observations.sqlite3
python create_tables.py
python initialize_database.py
python parse_csv.py
echo ""
python run_tests.py
